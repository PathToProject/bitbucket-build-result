#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
STATUS_NAME=${STATUS_NAME:?'STATUS_NAME variable missing.'}
STATUS_URL=${STATUS_URL:?'STATUS_URL variable missing.'}
STATUS_USER=${STATUS_USER:?'STATUS_USER variable missing.'}
STATUS_TOKEN=${STATUS_TOKEN:?'STATUS_TOKEN variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}
DESCRIPTION=${DESCRIPTION:="Build status from pipe"}

tee /tmp/data0.json << END
{
    "state": "SUCCESSFUL",
    "key": "${BITBUCKET_PROJECT_KEY}",
    "name": "${STATUS_NAME}",
    "url": "${STATUS_URL}",
    "description": "${DESCRIPTION}"
}
END

run curl --silent --show-error -u ${STATUS_USER}:${STATUS_TOKEN} -H "Content-Type: application/json" -X POST https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/commit/${BITBUCKET_COMMIT}/statuses/build -d @/tmp/data0.json

if [[ "${status}" == "0" ]]; then
  echo " :) "
  success "Success!"
else
  echo " :( "
  fail "Error!"
fi
