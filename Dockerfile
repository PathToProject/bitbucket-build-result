FROM alpine:3.14.0

RUN apk add --update --no-cache bash curl

COPY pipe /
COPY LICENSE.txt pipe.yml /
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh

RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
